process.env.NODE_ENV = 'localDevelopment';
config = require('config');
constant = require('./routes/constant');
var mysql = require('mysql');
connection = require('./routes/database');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var geolib = require('geolib');
var gcm = require('multi-gcm');
var routes = require('./routes/index');
var login = require('./routes/login');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//jade
app.use('/test', routes);
app.get('/recover_user_login',routes);
app.get('/recover_driver_login',routes);
//routers
app.post('/register_user',login);
app.post('/login_user',login);
app.post('/forget_user',login);
app.post('/recover_user',login);
app.post('/change_user_password',login);
app.post('/register_driver',login);
app.post('/login_driver',login);
app.post('/forget_driver',login);
app.post('/recover_driver',login);
app.post('/change_driver_password',login);
app.post('/find_drivers',login);
app.post('/nearest_driver',login);
app.post('/accept_order',login);
app.post('/reject_order',login);
app.post('/login_admin',login);
app.post('/change_status',login);
app.post('/delete_driver',login);
app.post('/verify_driver',login);
app.post('/unverify_driver',login);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

//module.exports = app;
