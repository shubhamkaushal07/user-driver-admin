var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('test');
});

router.get('/test', function(req, res, next) {
  res.render('test');
});
router.get('/recover_user_login', function(req,res,next){
  res.render('recover_user_login');
});

router.get('/recover_driver_login', function(req,res,next){
  res.render('recover_driver_login');
});
module.exports = router;