/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 101);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 102);
define(exports.responseStatus, "SHOW_MESSAGE", 103);
define(exports.responseStatus, "SHOW_DATA", 104);
exports.responseMessage = {}; define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN", "Please login again, Invalid access.");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "");
define(exports.responseMessage, "PASSWORD_DIDNT_MATCH", "Please confirm the password");
define(exports.responseMessage, "EMAIL_ALREADY_EXISTS","This email is already registered");
define(exports.responseMessage, "INVALID_LOGIN_DETAILS","The login details are not correct");
define(exports.responseMessage, "INVALID_ID","Enter valid id");
define(exports.responseMessage, "ORDER_STATUS","Order is confirmed");