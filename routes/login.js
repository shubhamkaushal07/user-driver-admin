/**
 * Created by Shubham on 10/29/2015.
 */
var express = require('express');
var mysql =  require('mysql');
var router = express.Router();
var random = require('random-key');
var async = require('async');
var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
var geolib = require('geolib');

// users registers
router.post('/register_user',function(req,res,next) {
    var fname = req.body.fname;
    var lname = req.body.lname;
    var email = req.body.email;
    var password = req.body.password;
    var cpassword = req.body.cpassword;
    var tablename = 'user_login';
    var userValues=[fname,lname,email,password,cpassword];
    async.waterfall([
        function(callback){
          func.checkBlank(res,userValues,callback);
        },
        function(callback){
          func.checkPasswordMatch(res,password,cpassword,callback);
        },
        function(callback){
          func.checkEmail(res,email,tablename,callback);
        }],
        function(err)
        {
          var key = random.generate();
          var add = "Insert into user_login (first_name,last_name,email,password,access_token) Values(?,?,?,?,'" + key + "')";
          connection.Query(add,userValues, function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err,res);
                }
                else {
                    var data = {
                        first_name: fname,
                        last_name: lname,
                        access_token: key
                    };
                    sendResponse.sendSuccessData(data, res);
                }

          });
        }
    )
});

//user login
router.post('/login_user',function(req,res) {
    var email = req.body.email;
    var password = req.body.password;
    var credentials = [email,password];
    var tablename = "user_login";
    async.waterfall([
        function(callback){
            func.checkBlank(res,credentials,callback);
        },
        function(callback){
            func.validAccount(res,email,password,tablename,callback);
        }],
        function(err,result){
            sendResponse.sendSuccessData(result[0].access_token,res);
        }
    )
});
//user_forget
router.post('/forget_user',function(req,res){
    var email = req.body.email;
    var tablename = "user_login";
    var values = [email];
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        }],
        function(err){
            func.sendRecoveryEmail(res,email,tablename);
        }
    );
});

//new password user
router.post('/recover_user',function(req,res){
    var otp = req.body.otp;
    var password = req.body.password;
    var cpassword = req.body.cpassword;
    var tablename = "user_login";
    var values = [otp,password,cpassword];
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        },
        function(callback){
            func.checkPasswordMatch(res,password,cpassword,callback);
        },
        function(callback){
            func.checkOtp(res,otp,tablename,callback);
        }],
        function(callback){
            func.changePassword(res,password,otp,tablename,callback);
        }
    );
});
// reset user password
router.post('/change_user_password',function(req,res){
    var accessToken = req.body.access_token;
    var oldpassword = req.body.opassword;
    var newpassword  = req.body.newpassword;
    var cnewpassword = req.body.cnewpassword;
    var tablename = "user_login";
    var values = [accessToken,oldpassword,newpassword,cnewpassword];
    async.waterfall([
            function(callback){
                func.checkBlank(res,values,callback);
            },
            function(callback){
                func.checkPasswordMatch(res,newpassword,cnewpassword,callback);
            },
            function(callback){
                func.checkKey(res,accessToken,tablename,callback);
            }],
        function(err,mail) {
            var email = mail[0].email;
            var change = "UPDATE " + tablename + " SET password='" + newpassword + "' where email= '"+email+"' and password='"+oldpassword+"'";
            connection.Query(change, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else if(result.affectedRows){
                    var msg = "Your password has been reset";
                    sendResponse.successStatusMsg(msg, res);
                }
                else
                {
                    sendResponse.sendErrorMessage(constant.responseMessage.INVALID_LOGIN_DETAILS,res);
                }
            });
        }
    );
});
// driver register
router.post('/register_driver',function(req,res) {
    var fname = req.body.fname;
    var lname = req.body.lname;
    var email = req.body.email;
    var password = req.body.password;
    var cpassword = req.body.cpassword;
    var lat = req.body.lat;
    var lon = req.body.lon;
    var devicetype = req.body.device_type;
    var devicetoken = req.body.device_token;
    var status = 1;
    var tablename = "driver_login";
    var driverValues = [fname, lname, email, password, lat, lon,status,devicetype,devicetoken];
    async.waterfall([
        function (callback) {
            func.checkBlank(res, driverValues, callback);
        },
        function (callback) {
            func.checkPasswordMatch(res, password, cpassword, callback);
        },
        function (callback) {
            func.checkEmail(res, email, tablename, callback);
        }],
        function (err) {
            var key = random.generate();
            var query1 = "Insert into driver_login (first_name,last_name,email,password,latitude,longitude,access_token,status,device_type,device_token) Values(?,?,?,?,?,?,'" + key + "',?,?,?)";
            connection.Query(query1, driverValues, function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var data = {
                    first_name: fname,
                    last_name: lname,
                    access_token: key

                    };
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
});

//driver login
router.post('/login_driver',function(req,res) {
    var email = req.body.email;
    var password = req.body.password;
    var tablename = "driver_login";
    var credentials = [email,password];
    async.waterfall([
        function(callback){
            func.checkBlank(res,credentials,callback);
        },
        function(callback){
            func.validAccount(res,email,password,tablename,callback);
        }],
        function(err,result){
            var query2 ="Select * from "+tablename+" where access_token='"+result[0].access_token+"'";
            connection.Query(query2,[],function(err,result) {
                if (err) {
                    sendResponse.sendErrorMessage(err,res);
                }
                else{
                    var data = {
                        first_name:result[0].first_name,
                        last_name:result[0].last_name,
                        latitude:result[0].latitude,
                        longitude:result[0].longitude,
                        access_token:result[0].access_token,
                        status:result[0].status
                    };

                    sendResponse.sendSuccessData(data,res);
                }
            });
        }
    );
});
// driver_forget
router.post('/forget_driver',function(req,res){
    var email = req.body.email;
    var tablename = "driver_login";
    var values = [email];
    async.waterfall([
            function(callback){

                func.checkBlank(res,values,callback);
            }],
        function(err){
            func.sendRecoveryEmail(res,email,tablename);
        }
    );
});
//new password driver
router.post('/recover_driver',function(req,res){
    var otp = req.body.otp;
    var password = req.body.password;
    var cpassword = req.body.cpassword;
    var tablename = "driver_login";
    var values = [otp,password,cpassword];
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        },
        function(callback){
            func.checkPasswordMatch(res,password,cpassword,callback);
        },
        function(callback){
            func.checkOtp(res,otp,tablename,callback);
        }],
        function(callback){
            func.changePassword(res,password,otp,tablename,callback);
        }
    );
});
// reset driver password
router.post('/change_driver_password',function(req,res){
    var accessToken = req.body.access_token;
    var oldpassword = req.body.opassword;
    var newpassword  = req.body.newpassword;
    var cnewpassword = req.body.cnewpassword;
    var tablename = "driver_login";
    var values = [accessToken,oldpassword,newpassword,cnewpassword];
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        },
        function(callback){
            func.checkPasswordMatch(res,newpassword,cnewpassword,callback);
        },
        function(callback){
            func.checkKey(res,accessToken,tablename,callback);
        }],
        function(err,mail) {
            var email = mail[0].email;
            var change = "UPDATE " + tablename + " SET password='" + newpassword + "' where email= '"+email+"' and password='"+oldpassword+"' ";
            connection.Query(change, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else if(result.affectedRows){
                    var msg = "Your password has been reset";
                    sendResponse.successStatusMsg(msg, res);
                }
                else{
                    sendResponse.sendErrorMessage(constant.responseMessage.INVALID_LOGIN_DETAILS,res);
                }
            });
        }
    );
});

//find drivers
router.post('/find_drivers',function(req,res){
    var key = req.body.access;
    var lat = req.body.lat;
    var lon = req.body.lon;
    var rad = req.body.rad;
    var values = [key,lat,lon,rad];
    var tablename = "user_login";
    var count = "count_drivers";
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        },
        function(callback){
            func.checkKey(res,key,tablename,callback);
        }],
        function(err,usermail){
            var driver = "SELECT first_name,last_name,email,latitude,longitude,status,device_type,device_token from driver_login where status=1";
            connection.Query(driver,[],function(err,result){
                 if(err){
                     sendResponse.sendErrorMessage(err,res);
                 }
                 else{
                     var data=[{}];
                     var reachables=[{}];
                     var count=0;
                     var arrlength = result.length;
                     for (var i = 0; i < arrlength; i++){
                           var a=  func.checkForNearbyDrivers(res,result[i],lat,lon,rad);
                           if(a==true) {
                               data[count]={
                                   first_name: result[i].first_name,
                                   last_name: result[i].last_name,
                                   email: result[i].email,
                                   latitude: result[i].latitude,
                                   longitude: result[i].longitude,
                                   status: result[i].status
                               };
                               reachables[count]={
                                   first_name: result[i].first_name,
                                   last_name: result[i].last_name,
                                   email: result[i].email,
                                   latitude: result[i].latitude,
                                   longitude: result[i].longitude,
                                   device_type: result[i].device_type,
                                   device_token: result[i].device_token,
                                   status: result[i].status
                               };
                               count++;
                           }
                      }
                     func.sendNotification(res,reachables,usermail[0].email);

                     data[count]={No_of_Drivers: count};
                     sendResponse.sendSuccessData(data,res);
                     }
            });
        }
    );
});

// nearest driver
router.post('/nearest_driver',function(req,res) {
    var accesstoken = req.body.access;
    var lat = req.body.lat;
    var lon = req.body.lon;
    var values = [accesstoken, lat, lon];
    var tablename = "user_login";
    async.waterfall([
        function (callback) {
            func.checkBlank(res, values, callback);
        },
        function (callback) {
            func.checkKey(res, accesstoken, tablename, callback);
        }],
        function (err, usermail) {
            var driver = "SELECT first_name,last_name,email,latitude,longitude,status,device_type,device_token from driver_login where status=1";
            connection.Query(driver, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var data = [{}];
                    var count=0;
                    var arrlength = result.length;
                    for (var i = 0; i < arrlength; i++) {
                        data[count] = {
                            first_name: result[i].first_name,
                            last_name: result[i].last_name,
                            email: result[i].email,
                            device_type: result[i].device_type,
                            device_token: result[i].device_token,
                            status: result[i].status,
                            latitude: result[i].latitude,
                            longitude: result[i].longitude
                        };
                        count++;
                    }
                    nearest = func.checkForNearestDriver(res,data,lat,lon);
                    nearestdriverdata = [nearest.driverdata];
                }
                var nearestinfo = {
                    first_name: nearest['driverdata'].first_name,
                    last_name: nearest['driverdata'].last_name,
                    email: nearest['driverdata'].email,
                    status: nearest['driverdata'].status,
                    latitude: nearest['driverdata'].latitude,
                    longitude: nearest['driverdata'].longitude,
                    distance: geolib.convertUnit('km',nearest.distance,2)
                };
                func.sendNotification(res,nearestdriverdata,usermail[0].email);
                sendResponse.sendSuccessData(nearestinfo,res);

            });
        });
});

// accept order by driver
router.post('/accept_order',function(req,res){
    var user_mail=req.body.email;
    var driver_access=req.body.access_token;
    var values=[user_mail,driver_access];
    var usertable='user_login';
    var drivertable='driver_login';
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        },
        function(callback){
            func.checkKey(res,driver_access,drivertable,callback);
        }],
        function(drivermail,err){
            var query="Select id from "+usertable+" where email='"+user_mail+"' and order_status=1";
            connection.Query(query,[],function(err,result){
                if(err){
                    sendResponse.sendErrorMessage(err,res);
                }
                else if(result.length){
                    var msg = "Order already done";
                    sendResponse.sendErrorMessage(msg,res);
                }
                else {
                    var change = "UPDATE " + usertable + " SET order_status=1 where email='" + user_mail + "' ";
                    connection.Query(change, [], function (err, result) {
                        if (err) {
                            sendResponse.sendErrorMessage(err, res);
                        }
                        else if(result.affectedRows) {
                            sendResponse.successStatusMsg(constant.responseMessage.ORDER_STATUS,res);
                        }
                        else{
                            sendResponse.somethingWentWrongError(res);
                        }
                    });
                }
            });
        }
    );
});
// reject order by driver
router.post('/reject_order',function(req,res){
    var user_mail=req.body.email;
    var driver_access=req.body.access_token;
    var values=[user_mail,driver_access];
    var usertable='user_login';
    var drivertable='driver_login';
    async.waterfall([
        function(callback){
            func.checkBlank(res,values,callback);
        },
        function(callback){
            func.checkKey(res,driver_access,drivertable,callback);
        }],
        function(drivermail,err){
            var query="Select id from "+usertable+" where email='"+user_mail+"' and order_status=1";
            connection.Query(query,[],function(err,result){
                if(err){
                    sendResponse.sendErrorMessage(err,res);
                }
                else if(result.length){
                    var msg = "Order already done";
                    sendResponse.sendErrorMessage(msg,res);
                }
                else {
                    var change = "UPDATE " + usertable + " SET order_status=0 where email='" + user_mail + "' ";
                    connection.Query(change, [], function (err, result2) {
                        if (err) {
                            sendResponse.sendErrorMessage(err, res);
                        }
                        else if (result2.affectedRows) {
                            sendResponse.successStatusMsg(constant.responseMessage.ORDER_STATUS,res);
                        }
                    });
                }
            });
        }
    );
});

//admin login and Display
router.post('/login_admin',function(req,res){
    var email = req.body.email;
    var password = req.body.password;
    var tablename = "admin_login";
    async.waterfall([
        function(callback){
            func.validAccount(res,email,password,tablename,callback);
        }],
        function(err,key){
            var query3 = "Select * from driver_login where 1";
            connection.Query(query3,[],function(err,result) {
                if (err) {
                    sendResponse.sendErrorMessage(err,res);
                }
                else{
                    var data = {
                        driver_data: result,
                        admin_access_token: key[0].access_token
                    };
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
});
//Driver status change
router.post('/change_status',function(req,res){
    var admintoken=req.body.admin_token;
    var drivertoken=req.body.driver_token;
    var keys = [admintoken,drivertoken];
    async.waterfall([
        function(callback){
            func.checkBlank(res,keys,callback);
        },
        function(callback){
            func.checkKey(res,admintoken,"admin_login",callback);
        },
        function(email,callback){
            func.checkKey(res,drivertoken,"driver_login",callback);
        }],
        function(err,email) {
            var status = "UPDATE driver_login SET status=NOT(status) where access_token= '" + drivertoken + "' ";
            connection.Query(status, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var data = {
                        driver_Email:email[0].email,
                        status:'changed successfully'

                    };
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
});
// verify driver
router.post('/verify_driver',function(req,res){
    var admintoken=req.body.admin_token;
    var driverid=req.body.driver_id;
    var keys = [admintoken,driverid];
    var drivertable = "driver_login";
    async.waterfall([
        function(callback){
            func.checkBlank(res,keys,callback);
        },
        function(callback){
            func.checkKey(res,admintoken,"admin_login",callback);
        },
        function(email,callback){
            func.checkId(res,driverid,drivertable,callback);
        }],
        function(email,callback){
            var verify = "UPDATE "+drivertable+" SET status=1 where id='"+driverid+"'";
            connection.Query(verify, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var data = {
                        driver_Email:email,
                        verification_Status:'verified'

                    };
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
});
//Unverify a driver
router.post('/unverify_driver',function(req,res){
    var admintoken=req.body.admin_token;
    var driverid=req.body.driver_id;
    var keys = [admintoken,driverid];
    var drivertable = "driver_login";
    async.waterfall([
        function(callback){
            func.checkBlank(res,keys,callback);
        },
        function(callback){
            func.checkKey(res,admintoken,"admin_login",callback);
        },
        function(email,callback){
            func.checkId(res,driverid,drivertable,callback);
        }],
        function(email,callback){
            var verify = "UPDATE "+drivertable+" SET status=2 where id='"+driverid+"'";
            connection.Query(verify, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var data = {
                        driver_Email:email,
                        verification_Status:'Not a verified driver'

                    };
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
});
//delete driver
router.post('/delete_driver',function(req,res){
    var admintoken=req.body.admin_token;
    var drivertoken=req.body.driver_token;
    var keys = [admintoken,drivertoken];
    async.waterfall([
        function(callback){
            func.checkBlank(res,keys,callback);
        },
        function(callback){
            func.checkKey(res,admintoken,"admin_login",callback);
        },
        function(email,callback){
            func.checkKey(res,drivertoken,"driver_login",callback);
        }],
        function(err,email) {
            var deletedriver = "DELETE from driver_login where access_token='" + drivertoken + "' ";
            connection.Query(deletedriver, [], function (err, result) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var data = {
                        Deleted_Driver_Email: email[0].email
                    };
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
});


module.exports = router;