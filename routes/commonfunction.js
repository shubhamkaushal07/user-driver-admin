var sendResponse = require('./sendResponse');
var mysql =  require('mysql');
var geolib = require('geolib');
var nodemailer = require('nodemailer');
var random = require('random-key');
var gcm = require('multi-gcm');
var MultiGCM = new gcm.MultiGCM('AIzaSyCtRs4zCcJWT9d9fMutZcrLZzBdxxYY8Xs');
/*
 * ------------------------------------------------------
 * Check if manadatory fields are not filled
 * INPUT : array of field names which need to be mandatory
 * OUTPUT : Error if mandatory fields not filled
 * ------------------------------------------------------
 */


exports.checkBlank = function (res, userValues, callback) {
    var checkBlankData = checkBlank(userValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '') {
            return 1;
            break;
        }
        else if (arr[i] == undefined) {
            return 1;
            break;
        }
        else if (arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkPasswordMatch = function (res,password,cpassword,callback) {

    if(password==cpassword){
        callback();
    }
    else{
        sendResponse.sendErrorMessage(constant.responseMessage.PASSWORD_DIDNT_MATCH,res);
    }
};

exports.checkEmail = function (res,email,tablename,callback){
    var checkmail = "Select * from "+tablename+" where email='"+email+"' ";
    connection.Query(checkmail,[], function (err, result) {
        if (err) {
            console.log(err);
            sendResponse.sendErrorMessage(err,res);
        }
        else if(result.length>0){
            sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_ALREADY_EXISTS,res);
        }
        else {
            callback();
        }

    });
};

exports.validAccount = function (res,email,password,tablename,callback) {
    var auth = "Select access_token from "+tablename+" where email='"+email+"' AND password='"+password+"'";
    connection.Query(auth,[], function (err, result) {
        if (err) {
            sendResponse.sendErrorMessage(err,res);
        }
        else if(result.length>0){
            callback(null,result);
        }
        else{
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_LOGIN_DETAILS,res);
        }
    });
};

exports.checkKey = function (res,key,tablename,callback){
    var auth = "Select email from "+tablename+" where access_token= '"+key+"'";
    connection.Query(auth,[], function (err, result) {
        if(err) {
            sendResponse.sendErrorMessage(err,res);
        }
        else if(result.length>0){
            callback(null,result);
        }
        else{
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_LOGIN_DETAILS,res);
        }
    });
};
exports.checkId = function(res,id,tablename,callback){
    var auth = "SELECT email from "+tablename+" where id='"+id+"'";
    connection.Query(auth,[],function(err,result){
        if(err){
            sendResponse.sendErrorMessage(err,result);
        }
        else if(result.length)
        {
            callback(result[0].email);
        }
        else{
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_ID,res);
        }
    });
};
exports.checkOtp = function (res,otp,tablename,callback){
    var auth = "Select email from "+tablename+" where otp= '"+otp+"'";
    connection.Query(auth,[], function (err, result) {
        if(err) {
            sendResponse.sendErrorMessage(err,res);
        }
        else if(result.length>0){
            callback();
        }
        else{
            var msg = "Link Expired!!";
            sendResponse.successStatusMsg(msg,res);
        }
    });
};

exports.changePassword = function(res,password,otp,tablename,callback){
    var querychange = "UPDATE "+tablename+" set Password='" + password + "' where otp='" + otp + "'";
    connection.Query(querychange, [], function (err, result) {
        if (err) {
            sendResponse.sendErrorMessage(err, res);
        }
        else if(result.affectedRows) {
            var deleteotp = "Update " + tablename + " SET otp='' where otp= '" + otp + "' ";
            connection.Query(deleteotp, [], function (err, result2) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else if (result2.affectedRows) {
                    var msg= "Your Password has been successfully changed.";
                    sendResponse.successStatusMsg(msg,res);
                }
            });
        }
        else{
            sendResponse.somethingWentWrongError(res);
        }
    });
};
//
exports.checkForNearbyDrivers = function(res,result,lat,lon,rad){
    var bool = geolib.isPointInCircle(
        {latitude: result.latitude, longitude: result.longitude},
        {latitude: lat, longitude: lon},
        rad
    )
    return bool;
};

exports.checkForNearestDriver = function(res,drivers,lat,lon){
    var user = {"directions": {latitude: lat,longitude: lon}
    };
    var nearestdriver = geolib.findNearest(user['directions'],drivers, 0);
    for(var i=0;i<drivers.length;i++)
    {
        if(drivers[i].latitude==nearestdriver.latitude && drivers[i].longitude==nearestdriver.longitude){
            var drivers = {
                driverdata: drivers[i],
                distance: nearestdriver.distance
            };
            return drivers;
        }
    }
};
exports.sendNotification = function(res,drivers,usermail){
    var driver_device_token = [];
    var driver_device_type = [];
    var androidId=[];
    var iOSId=[];
    var device_type;
        for(var i=0;i<drivers.length;i++)
    {
        driver_device_type[i]=drivers[i].device_type;
        driver_device_token[i]=drivers[i].device_token;
        if(driver_device_type[i]==1)
        {
            androidId.push(driver_device_token[i]);
        }
        else if(driver_device_token[i]==2){
            iOSId.push(driver_device_token[i]);
        }
    }
    if(androidId.length) {
        var messageAndroid = new gcm.GCMMessage({
            registration_ids: androidId,
            type: 'Android', //Android or iOS
            data: {
                message: 'hello',
                customer_email: usermail
            },
            notification: {
                title: "Hello, World",
                icon: "ic_launcher",
                body: "This is a notification that will be displayed ASAP."
            }
        });
        MultiGCM.send(messageAndroid, function (err, res) {
            if (err) {
                sendResponse.sendErrorMessage(err,res);
            }
            else {
                console.log(res);
                //console.log(res.result);
            }
        });
    }
    if(iOSId.length) {
        var messageiOS = new gcm.GCMMessage({
            registration_ids: iOSId,
            type: 'iOS', //Android or iOS
            data: {}
        });
        MultiGCM.send(messageiOS, function (err, res) {
            if (err) {
                console.error(err, 'and');
            }
            else {
                console.log(res);
            }
        });
    }
};
//
exports.sendRecoveryEmail = function(res,email,tablename) {
    var auth = "SELECT access_token from " + tablename + " where email= '" + email + "' ";
    connection.Query(auth, [], function (err, result) {
        if (err) {
            sendResponse.sendErrorMessage(err, res);
        }
        else if (result.length) {
            var randomno = random.generate();
            var otp = "UPDATE " + tablename + " set otp = '" + randomno + "' where email= '" + email + "' ";
            connection.Query(otp, [], function (err, result2) {
                if (err) {
                    sendResponse.sendErrorMessage(err, res);
                }
                else {
                    var transporter = nodemailer.createTransport({
                        service: 'Gmail',
                        auth: {
                            user: 'shubham.kaushal@theroboticshomepage.com',
                            pass: 'click12345'
                        }
                    });
                    var mailOptions = {
                        from: 'Shubham  <shubham.kaushal@mail.click-labs.com>', // sender address
                        to: email, // list of receivers
                        subject: 'Forget Password', // Subject line
                        text: 'hello', // plaintext body
                        html: '<b> Link to change password : http://localhost:8000/recover_'+tablename+'?Access_token=' + randomno + ' </b>',
                    };

                    transporter.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            sendResponse.sendErrorMessage(err, res);
                        }
                        else {
                            var msg= "Recovery link is sent to your email";
                            sendResponse.successStatusMsg(msg, res);
                        }
                    });
                }
            });
        }
        else {
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_LOGIN_DETAILS,res);
        }
    });
};


